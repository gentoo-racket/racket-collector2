MAKE        := make
RACKET      := racket
RACO        := raco
SCRIBBLE    := $(RACO) scribble
SH          := sh
RM          := rm
RMDIR       := $(RM) -r

BINDIR      := $(PWD)/bin
DOCSDIR     := $(PWD)/docs
EXTRAS      := $(PWD)/extras
PUBLICDIR   := $(PWD)/public
SCRIPTSDIR  := $(PWD)/scripts
SRCDIR      := $(PWD)/src
TESTSDIR    := $(PWD)/tests


.PHONY: all
all: clean compile


src-make-%:
	$(MAKE) -C $(PWD)/src DEPS-FLAGS=" --no-pkg-deps " $(*)


.PHONY: clean
clean: src-make-clean

.PHONY: compile
compile: src-make-compile

.PHONY: install
install: src-make-install

.PHONY: setup
setup: install
setup: src-make-setup

.PHONY: test
test: install
test: src-make-test

.PHONY: remove
remove: src-make-remove


.PHONY: completion
completion:
	ziptie-make-completion -t zsh collector2 > $(EXTRAS)/completion/zsh/_collector2


public:
	mkdir -p $(PUBLICDIR)

.PHONY: docs-html
docs-html: public
	cd $(DOCSDIR) && $(SCRIBBLE) ++main-xref-in --dest $(PWD) \
		--dest-name public --htmls --quiet $(DOCSDIR)/scribblings/main.scrbl

.PHONY: docs-latex
docs-latex: public
	$(RACKET) $(SCRIPTSDIR)/doc.rkt latex

.PHONY: docs-markdown
docs-markdown: public
	$(RACKET) $(SCRIPTSDIR)/doc.rkt markdown

.PHONY: docs-pdf
docs-pdf: public
	$(RACKET) $(SCRIPTSDIR)/doc.rkt pdf

.PHONY: docs-text
docs-text: public
	$(RACKET) $(SCRIPTSDIR)/doc.rkt text

.PHONY: docs-public
docs-public: docs-html

.PHONY: docs-all
docs-all: docs-html docs-latex docs-markdown docs-pdf docs-text

.PHONY: clean-public
clean-public:
	if [ -d $(PUBLICDIR) ] ; then $(RMDIR) $(PUBLICDIR) ; fi

.PHONY: regen-public
regen-public: clean-public docs-public
