;; This file is part of racket-collector2 - ebuilds from Racket catalogs.

;; racket-collector2 is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-collector2 is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-collector2.  If not, see <https://www.gnu.org/licenses/>.

;; Original author: Maciej Barć <xgqt@riseup.net>
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later


#lang racket/base

(require racket/contract
         racket/date
         (only-in threading ~>>))

(provide (all-defined-out))


#|
Return a PMS style number for the patch (_p) version slot.

Example:
(epoch->patch 1622077200)  ; => 20210527
|#

(define/contract (epoch->patch seconds)
  (-> exact-nonnegative-integer? exact-nonnegative-integer?)
  (parameterize ([date-display-format 'iso-8601])
    (~>> seconds
         seconds->date
         date->string
         (regexp-replace* "-" _ "")
         string->number)))
