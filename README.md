# Collector 2

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/gentoo-racket/racket-collector2">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/gentoo-racket/racket-collector2/">
    </a>
    <a href="https://gitlab.com/gentoo-racket/racket-collector2/pipelines">
        <img src="https://gitlab.com/gentoo-racket/racket-collector2/badges/master/pipeline.svg">
    </a>
</p>


# About

Parse [Racket packages catalog](https://pkgs.racket-lang.org/)
and generate [ebuild scripts](https://wiki.gentoo.org/wiki/Ebuild).

## Online Documentation

You can read more documentation
[on GitLab pages](https://gentoo-racket.gitlab.io/racket-collector2/).


# Installation

## From Packages Catalog

```sh
raco pkg install collector2
```

## From repository

Quick install using Make

```sh
make -C src install
```

## Dependencies

- [counter](https://gitlab.com/xgqt/scheme-counter)
- [ebuild](https://gitlab.com/xgqt/racket-ebuild)
- [threading](https://github.com/lexi-lambda/threading)
- [upi](https://gitlab.com/xgqt/upi)


# License

Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

Original author: Maciej Barć <xgqt@riseup.net>

SPDX-License-Identifier: GPL-2.0-or-later
