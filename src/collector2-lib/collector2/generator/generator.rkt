;; This file is part of racket-collector2 - ebuilds from Racket catalogs.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; Original author: Maciej Barć <xgqt@riseup.net>
;; SPDX-License-Identifier: GPL-2.0-or-later

;; collector2 is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; collector2 is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with collector2.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/match
         racket/promise
         racket/string
         ebuild
         threading
         "../pkgs/pkgs.rkt"
         "make.rkt")

(provide packages
         repository
         ;; From pkgs.rkt
         hard-excluded
         soft-excluded
         verbose-filter?
         ;; From make.rkt
         architectures
         package-category)


;; URLs may contain a placeholder URL with "empty.zip"
;; (ie.: http://racket-packages.s3-us-west-2.amazonaws.com/pkgs/empty.zip),
;; this is why we have to find a URL without ".zip" if possible

(define (archive? str)
  (list? (regexp-match #rx".*.tar.*|.*.zip$" str)))

(define (data->src data)
  ;; Candidates for source URL.
  (let ([candidate-1
         (~> data
             (hash-ref 'versions (hash))
             (hash-ref 'default (hash))
             (hash-ref 'source ""))]
        [candidate-2
         (hash-ref data 'source "")])
    (cond
      [(or (archive? candidate-1) (equal? candidate-1 ""))
       (cond
         [(or (archive? candidate-2) (equal? candidate-2 ""))
          candidate-1]
         [else
          candidate-2])]
      [else
       candidate-1])))

(define (dep-name v)
  (cond
    [(string? v) v]
    [(list? v) (car v)]
    [else ""]))

(define (get-circular-dependency all-packages package-name package-data)
  (for/first
      ([dependency (hash-ref package-data 'dependencies '())]
       #:when
       (let ([dependency-dependencies
              (~> all-packages
                  (hash-ref (dep-name dependency) (hash))
                  (hash-ref 'dependencies '()))])
         (match dependency-dependencies
           [(list-no-order (list-no-order (== package-name) _ ...) _ ...)
            #true]
           [(list-no-order (== package-name) _ ...)
            #true]
           [_
            #false])))
    (dep-name dependency)))

(define (make-package name data all-packages)
  (let ([src (data->src data)]
        [circular (get-circular-dependency all-packages name data)])
    (cond
      [(and circular (string-contains? src "git"))
       (let ([aux-data (hash-ref all-packages circular (hash))])
         (make-cir name src data circular (data->src aux-data) aux-data))]
      [(string-contains? src "git")
       (make-gh name src data)]
      [else
       (make-archive name src data)])))


(define (packages [only-packages '()])
  (let ([all-packages
         (cond
           [(null? only-packages)
            (pkgs)]
           [else
            (for/hash ([(name data) (pkgs)]
                       #:when (member name only-packages))
              (values name data))])]
        [results '()]
        [active-workers '()]
        [make-worker
         (lambda (name data all-packages)
           (delay/thread (make-package name data all-packages)))])
    (for ([(name data) (in-hash all-packages)])
      (let ([len-active-workers (length active-workers)])
        (log-warning "[INFO] Making: ~v (currently running: ~a)~%"
                     name
                     len-active-workers)
        (cond
          [(>= len-active-workers 10)
           (let ([head-worker (car active-workers)])
             ;; Force the head-worker.
             (force head-worker)
             ;; Check if any other workers are ready.
             (set! active-workers
                   (for/fold ([running-workers '()]
                              #:result (reverse running-workers))
                             ([active-worker (in-list active-workers)])
                     (cond
                       ;; Worker is ready.
                       [(promise-forced? active-worker)
                        (set! results
                              `(,@results
                                ,(force active-worker)))
                        (values running-workers)]
                       ;; Worker is still running.
                       [else
                        (values (cons active-worker running-workers))])))
             ;; Then spawn another worker on the tail.
             (set! active-workers
                   `(,@active-workers
                     ,(make-worker name data all-packages))))]
          [else
           ;; Spawn.
           (set! active-workers
                 `(,@active-workers
                   ,(make-worker name data all-packages)))])))
    (for ([active-worker active-workers])
      (set! results
            `(,@results
              ,(force active-worker))))
    results))

(define (repository repository-modification-function [only-packages '()])
  (let ([repo
         (new repository%
              [name "racket-overlay"]
              [packages (packages only-packages)])])
    (repository-modification-function repo)
    repo))
