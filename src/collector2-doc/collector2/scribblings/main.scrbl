;; This file is part of racket-collector2 - ebuilds from Racket catalogs.
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; Original author: Maciej Barć <xgqt@riseup.net>
;; SPDX-License-Identifier: GPL-2.0-or-later

;; collector2 is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; collector2 is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with collector2.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require @(only-in scribble/bnf nonterm)
          @(only-in collector2/version VERSION)
          @(for-label racket/base))


@(define upstream-tree
   "https://gitlab.com/gentoo-racket/collector2/-/tree/")

@(define (link2overlay pth)
   (let ([overlay
          "https://gitlab.com/gentoo-racket/racket-overlay/-/tree/master/"])
     (link (string-append overlay pth) pth)))


@title{Collector2}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]

@table-of-contents[]

@index-section[]


@section{About}

Parse Racket catalogs and generate ebuild scripts.

Version: @link[@(string-append upstream-tree @VERSION)]{@VERSION}.


@subsection{Upstream}

The upstream repository can be found on
@link["https://gitlab.com/gentoo-racket/collector2" "GitLab"].


@section{Console usage}

@itemlist[
 @item{
  @Flag{C} @nonterm{url} or @DFlag{catalog} @nonterm{url}
  --- set the current-pkg-catalogs catalog to be examined
 }
 @item{
  @Flag{E} @nonterm{package} or @DFlag{exclude} @nonterm{package}
  --- exclude package from being generated,
  treat reverse dependencies as though the package did not exist
 }
 @item{
  @Flag{e} @nonterm{package} or @DFlag{exclude} @nonterm{package}
  --- exclude package and all packages depending on it from being generated
 }
 @item{
  @Flag{o} @nonterm{package} or @DFlag{only-package} @nonterm{package}
  --- only create/show the specified package
 }
 @item{
  @Flag{d} @nonterm{directory} or @DFlag{directory} @nonterm{directory}
  --- set the directory for @DFlag{create} to a given @nonterm{directory}
 }

 @item{
  @Flag{A} @nonterm{keywords} or @DFlag{architectures} @nonterm{keywords}
  --- architectures keywords (input as one string)
 }
 @item{
  @Flag{p} @nonterm{category} or @DFlag{package-category} @nonterm{category}
  --- set the category name to be used for generated packages
 }

 @item{
  @Flag{x} @nonterm{file-path} or @DFlag{excludes-file} @nonterm{file-path}
  --- path to a file with package names to exclude
  the excludes file should have the folowing syntax:
  @codeblock{
   ((hard "pkg_hard" ...) (soft "pkg_soft" ...))
  }
 }

 @item{
  @Flag{c} or @DFlag{create}
  --- create ebuilds in a directory (default to the current directory),
  can be overwritten by @DFlag{create-all-directory} flag
 }
 @item{
  @Flag{s} or @DFlag{show}
  --- dump ebuilds to standard out, do not write to the disk
 }

 @item{
  @DFlag{verbose-auto-catalog}
  --- show if automatically setting the Racket catalogs
 }
 @item{
  @DFlag{verbose-exclude}
  --- show manually excluded packages
 }
 @item{
  @DFlag{verbose-filter}
  --- show filtered packages
 }
 @item{
  @Flag{v} or @DFlag{verbose}
  --- increase verbosity (enable other verbosity switches)
 }

 @item{
  @Flag{h} or @DFlag{help}
  --- show help information with usage options
 }
 ]


@section{Repository generation}

@subsection{Missing files}

Collector2 only generates packages in the "dev-racket" category,
so to get a functional repository out of collector2's output you have
to also add in some files:

@itemlist[
 @item{@link2overlay{eclass}es used by generated ebuilds}
 @item{@link2overlay{profiles/categories}}
 @item{@link2overlay{profiles/repo_name}}
 @item{@link2overlay{metadata/layout.conf}}
 ]
